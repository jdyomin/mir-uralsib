$(function() {

    $('.scrollable').mCustomScrollbar();

    $(this).keydown(function(eventObject) {
      if (eventObject.which == 27) {
      }
    });

    $(document).mouseup(function (e) {
        var tooltip = $('.js-tooltip');
        if (tooltip.has(e.target).length === 0) {
            tooltip.removeClass('opened');
        }
    });

    function menuToggle() {
        $('.js-menu-trigger').toggleClass('active');
        $('.js-menu').toggleClass('opened');
        $('.page-header').toggleClass('opened');
    };

    $('.js-menu-trigger').on('click', menuToggle);
    $('.js-menu-trigger').on('click', function() {
        console.log('toggle');
    })
    $(document).on('click', '.js-menu.opened li.menuanchor', menuToggle);

    $('.js-menu li.menuanchor').on('click', function () {
        var self = $(this);

        setTimeout(function () {
            $('.js-menu li.menuanchor').removeClass('active');
            self.addClass('active');
        }, 2000);
    });

    $('#fullpage').fullpage({
        fixedElements: '#header',
        anchors: ['top', 'mechanics', 'gifts', 'winners'],
        menu: '#menu',
        fitToSection: true,
        keyboardScrolling: true,
        animateAnchor: true,
        verticalCentered: false,
        normalScrollElements: '.contacts-list',
        onLeave: function(index, nextIndex, direction) {
            var nextSection = getSectionByIndex(nextIndex);

            // if (index == 1 && direction == 'down') {
            //     $('#header').addClass('scrolled')
            //         .css('background-color', 'rgba(255, 255, 255, 1)');
            // } else if (nextIndex == 1 && direction == 'up') {
            //     $('#header').removeClass('scrolled')
            //         .css('background-color', 'rgba(255, 255, 255, 0)');
            // }

            var sect;

            if (direction == 'down') {
                if (!isVisible(nextSection)) {
                    sect = getSectionByIndex(index+2);
                } else {
                    sect = getSectionByIndex(index+1);
                }
            } else {
                if (!isVisible(nextSection)) {
                    sect = getSectionByIndex(index-2);
                } else {
                    sect = getSectionByIndex(index-1);
                }
            }

            var sectId = sect.data('anchor'),
                menu = $('.js-menu'),
                menuEls = menu.find('li.menuanchor');

            if (sectId && menu.find('[data-menuanchor="'+ sectId +'"]').length) {
                menuEls.removeClass('active');
                menu.find('[data-menuanchor="'+ sectId +'"]').addClass('active');
            } else if (!menu.find('[data-menuanchor="'+ sectId +'"]').length && sectId != 'gifts2') {
                menuEls.removeClass('active');
            }

            if(!isVisible(nextSection)) {
                var sectionIndex = getNearestVisibleSectionIndex(nextIndex, direction);

                if (sectionIndex !== -1) {

                    $.fn.fullpage.moveTo(sectionIndex);
                }

                return false;
            }

            function getSectionByIndex(index) {
                return $('.section').eq(index - 1);
            }

            function isVisible(section) {
                return $(section).is(':visible');
            }

            function getNearestVisibleSectionIndex(index, direction) {
                var step = direction === 'up' ? -1 : 1,
                    sectionsCount = $('.section').length;

                while (index >=0 && index < sectionsCount && !isVisible(getSectionByIndex(index))) {
                    index += step;
                }

                return index;
            }
        }
    });

    if ($(window).width() < 1024) {

        $('.menu-scrollable').mCustomScrollbar();
    }

    if ($(window).width() < 901) {
        $('.mobile-slider').addClass('swiper-container');
        $('.mobile-wrapper').addClass('swiper-wrapper');
        $('.mobile-slide').addClass('swiper-slide');

        var mySwiper = new Swiper('.mobile-slider', {
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            },
            observer: true
        });
    };

    if ($(window).width() < 801) {
        var winnersSlider = new Swiper('.winners-slider', {
            pagination: {
                el: '.swiper-pagination',
                type: 'fraction',
                clickable: true,
                renderFraction: function (currentClass, totalClass) {
                  return  'Победитель ' + '<span class="' + currentClass + '"></span>' +
                         ' из ' +
                         '<span class="' + totalClass + '"></span>';
              }
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            },
            observer: true
        });

        $('.table-mobile-prev').on('click', function() {
            winnersSlider.slidePrev();
        });

        $('.table-mobile-next').on('click', function() {
            winnersSlider.slideNext();
        });
    }
});




