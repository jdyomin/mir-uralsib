window.fbAsyncInit = function () {
    FB.init({
        appId: '1953796044880533',
        status: true,
        xfbml: true
    });
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "http://connect.facebook.net/en_US/all.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

Share = {
    vkontakte: function (purl, ptitle, pimg, text) {
        url = 'https://vk.com/share.php?';
        url += 'url=' + encodeURIComponent(purl);
        Share.popup(url);
    },
    odnoklassniki: function (purl, text, image) {
        url = 'https://connect.ok.ru/offer';
        url += '?url=' + encodeURIComponent(purl);
        Share.popup(url);
    },
    facebook: function (purl, ptitle, pimg, text) {

        FB.ui(
            {
                method: 'share',
                href: purl,
            }, function (response) {}
        );

    },
    twitter: function (purl, ptitle) {
        url = 'http://twitter.com/share?';
        url += 'text=' + encodeURIComponent(ptitle);
        url += '&url=' + encodeURIComponent(purl);
        url += '&counturl=' + encodeURIComponent(purl);
        Share.popup(url);
    },

    popup: function (url) {
        window.open(url, '', 'toolbar=0,status=0,width=626,height=800');
    }
};